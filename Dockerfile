FROM alpine:latest

ADD https://downloads.rclone.org/rclone-current-linux-amd64.zip /tmp/rclone/

RUN apk add --no-cache ca-certificates

WORKDIR /tmp/rclone

RUN mkdir extract
RUN unzip rclone-current-linux-amd64.zip -d extract
RUN cp extract/*/rclone /usr/bin/rclone
RUN cd / && rm -rf /tmp/rclone

WORKDIR /root

